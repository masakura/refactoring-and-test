import { Rental } from './rental'

export class Customer {
  private readonly rentals: Rental[] = []

  constructor (readonly name: string) {
  }

  add (rental: Rental): void {
    this.rentals.push(rental)
  }

  statement () {
    let totalAmount = 0
    let result = `Rental record for ${this.name}\n\n`

    for (const rental of this.rentals) {
      let amount: number

      switch (rental.movie.priceCode) {
        case 'Regular':
          amount = 300
          if (rental.daysRented > 2) amount += (rental.daysRented - 2) * 200
          break

        case 'NewRelease':
          amount = rental.daysRented * 300
          break

        case 'Kids':
          amount = 150
          if (rental.daysRented > 3) amount += (rental.daysRented - 3) * 150
          break
      }

      totalAmount += amount
      result += `\t${rental.movie.title}\t${amount}\n`
    }

    result += '\n'
    result += `Amount owd with ${totalAmount}`

    return result
  }
}
