import { Customer } from '../customer'
import { Rental } from '../rental'
import { Movie } from '../movie'

describe('Customer', () => {
  test('print receipt.', () => {
    const customer = new Customer('Taro')
    customer.add(new Rental(new Movie('STAR WARS EPISODE III', 'Regular'), 2))

    expect(customer.statement()).toEqual(`
Rental record for Taro

\tSTAR WARS EPISODE III\t300

Amount owd with 300
`.trim())
  })
})
