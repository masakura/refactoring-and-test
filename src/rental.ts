import { Movie } from './movie'

export class Rental {
  constructor (readonly movie: Movie, readonly daysRented: number) {
  }
}
