import { PriceCode } from './price-code'

export class Movie {
  constructor (readonly title: string, readonly priceCode: PriceCode) {
  }
}
