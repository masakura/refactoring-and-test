# リファクタリング (TypeScript)

書籍[リファクタリング 第一版]()から簡略化したものを題材にしています。

* ポイントを省きました
* 金額を $ から円にしています
* 言語を TypeScript に変えました

## 仕様

| Type         | Price                    |
|--------------|--------------------------|
| `Regular`    | 二泊まで 200 円、それ以降は 200 円/泊 |
| `NewRelease` | 300 円/泊                  |
| `Kids`       | 三泊まで 150 円、それ以降は 150 円/泊 |

こういうレシートを表示する。

```
Rental record for Taro

	STAR WARS EPISODE III	300

Amount owd with 300
```

## クラス図

```mermaid
classDiagram
  class Customer {
    +name: string
    +add(rental: Rental)
    +statement() string
  }
  
  class Rental {
    +movie: Movie
    +daysRented: number
  }
  
  class Movie {
    +title: string
    +priceCode: PriceCode
  }
  
  Customer *-- Rental
  Rental -- Movie
```

最終的に...

```mermaid
classDiagram
  class Customer {
    +name: string
    +add(rental: Rental)
    +statement() string
    -totalAmount() number
  }
  
  class Rental {
    +movie: Movie
    +daysRented: number
    +getCharge(): number
  }
  
  class Movie {
    +title: string
    +price: Price
    +getCharge(days: number) number
  }
  
  class Price {
    <<abstract>>
    +getCharge(days: number) number
  }
  
  class RegularPrice {
  }
  
  class NewReleasePrice {
  }
  
  class KidsPrice {
  }
  
  Customer *-- Rental
  Rental -- Movie
  Movie -- Price
  
  Price <|-- RegularPrice
  Price <|-- NewReleasePrice
  Price <|-- KidsPrice
```
