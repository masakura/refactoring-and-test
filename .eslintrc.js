module.exports = {
  env: {
    commonjs: true,
    es2021: true,
    node: true,
    'jest/globals': true
  },
  extends: [
    'standard'
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 'latest'
  },
  plugins: [
    '@typescript-eslint',
    'jest'
  ],
  rules: {
    'no-useless-constructor': 'off'
  },
  ignorePatterns: [
    'node_modules',
    'package*.json'
  ]
}
